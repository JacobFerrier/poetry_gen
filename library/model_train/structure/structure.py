import os

def index_of_structure_in_structures(test_structure, structures):
    for index in structures:
        structure = structures[index]['structure']
        if test_structure == structure:
            return index

    return None

def get_unigram_probability_of_structure(structure, filtered_structures):
    index = index_of_structure_in_structures(structure, filtered_structures)
    if index:
        return filtered_structures[index]['unigram_probability']
    else:
        return 0

def get_most_likely_following_structure(structure, haikus, filtered_structures):
    # find structure in all but the last index of pos_structure and build a bigram frequency model off of
    # immediate following structure in pos_structure
    following_structures, following_structures_counts = [], []
    for haiku_index in haikus:
        pos_structure = haikus[haiku_index]['pos_structure']
        for temp in pos_structure[:-1]:
            if structure == temp:
                following_structure = pos_structure[pos_structure.index(structure)+1]
                if following_structure in following_structures:
                    following_structures_counts[following_structures.index(following_structure)] += 1
                else:
                    following_structures.append(following_structure)
                    following_structures_counts.append(1)

    following_structures_probabilities = [get_unigram_probability_of_structure(x, filtered_structures)*(following_structures_counts[following_structures.index(x)]/sum(following_structures_counts)) for x in following_structures]

    max_probability = max(following_structures_probabilities)
    if max_probability == 0:
        return ValueError("all following structure probabilities are 0")
    else:
        most_likely_structure = following_structures[following_structures_probabilities.index(max_probability)]
        return (most_likely_structure, max_probability)

def get_filtered_structures(haikus, filter_factor):
    # get distinct structures and their counts
    structures = {}
    structures[0] = {'structure': [], 'count': 0}
    for haiku_index in haikus:
        for individual_structure in haikus[haiku_index]['pos_structure']:
            index = index_of_structure_in_structures(individual_structure, structures)
            if index:
                structures[index]['count'] += 1
            else:
                structures[len(structures)] = {'structure': individual_structure, 'count': 1}

    # get list of counts
    counts = []
    for structure_index in structures:
        counts.append(structures[structure_index]['count'])

    
    # get permissible count by setting a threshold count that is the minimum of the top 1% of counts
    sorted_counts = sorted(counts, reverse = True)
    permissible_count = sorted_counts[round(len(sorted_counts) * filter_factor)]

    # filter structures by their counts allowing only the top 10% counts in
    filtered_structures = {}
    filted_total_count = 0
    for structure_index in structures:
        if structures[structure_index]['count'] >= permissible_count:
            filtered_structures[structure_index] = structures[structure_index]
            filted_total_count += structures[structure_index]['count']

    # filtered_structures add unigram probabilities to filtered structures
    for structure_index in filtered_structures:
        filtered_structures[structure_index]['unigram_probability'] = filtered_structures[structure_index]['count']/filted_total_count    

    return filtered_structures

def subset_haikus(haikus, term, condition):
    subsetted_haikus = {}
    for haiku_index in haikus:
        if haikus[haiku_index][term] == condition:
            subsetted_haikus[haiku_index] = haikus[haiku_index]

    return subsetted_haikus

# create filtered sets based on the input haikus as well as a filter factor, which creates an artificial cutoff for probabilisticly low parts of speech (pos) structures (sequence of parts of speeches)
def __main__(MAINDIR, haikus_in, model_condition_table, filter_factor):
    set_filtered_structures = {}
    for model_name in model_condition_table:
        term = model_name + '_prediction'
        for condition in model_condition_table[model_name]:
            haiku_subset = subset_haikus(haikus_in, term, condition)
            set_filtered_structures[str(model_name) + '_' + str(condition)] = {'structures': get_filtered_structures(haiku_subset, filter_factor), 'haikus': haiku_subset}
    
    return set_filtered_structures