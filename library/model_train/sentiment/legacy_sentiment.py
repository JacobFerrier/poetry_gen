import os, re, nltk
from nltk import word_tokenize
from sklearn.naive_bayes import GaussianNB
from library.common import common
    
def get_vocabulary_semeval_2007(corpus):
    vocabulary = []
    with open(corpus) as file:
        for line in file:
            m = re.match(r'<instance id="[0-9]+">(.*)<\/instance>', line)
            if m:
                stream = m.groups()[0].lower()
                tokens = [x.replace(".", "") for x in word_tokenize(stream)]
                for token in tokens:
                    word = common.get_valid_token(token)
                    if word:
                        vocabulary.append(word)

    file.close()

    return vocabulary

def get_setence_scores(corpus):
    sentence_scores = {}
    with open(corpus) as file:
        for line in file:
            seq = line.strip().split()
            score_vector = [ int(x) for x in seq[1:]]
            sentence_scores[int(seq[0])] = score_vector

    return sentence_scores

def get_sentence_vectors(corpus, corpus_scores, lexicon):
    sentences = {}
    with open(corpus) as file:
        for line in file:
            m_id = re.match(r'<instance id="(\d+)">', line)
            if m_id:
                sentence_id = int(m_id.groups()[0])
                
                word_list = []
                m = re.match(r'<instance id="[0-9]+">(.*)<\/instance>', line)
                if m:
                    stream = m.groups()[0].lower()
                    tokens = [x.replace(".", "") for x in word_tokenize(stream)]
                    for token in tokens:
                        word = common.get_valid_token(token)
                        if word:
                            word_list.append(word)

                word_counts = {}
                for word in word_list:
                    if word not in word_counts.keys(): word_counts[word] = 1
                    else: word_counts[word] += 1
                
                sentence_bow_form = []
                sentence_vector_form = [0 for x in range(len(lexicon))]
                for word in word_counts:
                    sentence_bow_form.append((lexicon[word], word_counts[word]))
                    sentence_vector_form[lexicon[word]] = word_counts[word]

                sentences[sentence_id] = (corpus_scores[sentence_id], sentence_bow_form, sentence_vector_form)

    file.close()

    return sentences

def __main__(MAINDIR):
    os.chdir(MAINDIR + '/data/model_train/sentiment')

    #build global vocabulary index used for creating vectors of sentences
    vocabulary_global = get_vocabulary_semeval_2007(os.getcwd()+'/semeval_2007/AffectiveText.trial/affectivetext_trial.xml') + get_vocabulary_semeval_2007(os.getcwd()+'/semeval_2007/AffectiveText.test/affectivetext_test.xml')
    lexicon_global_set = set(vocabulary_global)
    lexicon_global, id_counter = {}, 0
    for token in lexicon_global_set:
        lexicon_global[token] = id_counter
        id_counter += 1

    #get scores for training set and testing set
    scores_train = get_setence_scores(os.getcwd()+'/semeval_2007/AffectiveText.trial/affectivetext_trial.emotions.gold')
    scores_test = get_setence_scores(os.getcwd()+'/semeval_2007/AffectiveText.test/affectivetext_test.emotions.gold')
    
    #get sentence id's, scores, bow representatives and vector representatives for training set
    sentences_train = get_sentence_vectors(os.getcwd()+'/semeval_2007/AffectiveText.trial/affectivetext_trial.xml', scores_train, lexicon_global)
    sentences_test = get_sentence_vectors(os.getcwd()+'/semeval_2007/AffectiveText.test/affectivetext_test.xml', scores_test, lexicon_global)

    #get train_x_vectors and train_y_targets
    sorted_ids_train = sorted(list(sentences_train.keys()))
    train_x_vectors, train_y_targets = [], []
    for i in sorted_ids_train:
        train_x_vectors.append(sentences_train[i][2])
        
        #this adds the vector of values to target
        #train_y_targets.append(sentences_train[i][0])

        #this adds the index of the highest scoring emotion to target where (0, 1, 2, 3, 4, 5) = (anger, disgust, fear, joy, sadness, surprise)
        score_vector = sentences_train[i][0]
        train_y_targets.append(score_vector.index(sorted(score_vector, reverse = True)[0]))

    #get test_x_vectors and test_y_targets
    sorted_ids_test = sorted(list(sentences_test.keys()))
    test_x_vectors, test_y_targets = [], []
    for i in sorted_ids_test:
        test_x_vectors.append(sentences_test[i][2])
        
        #this adds the vector of values to target
        #test_y_targets.append(sentences_test[i][0])

        #this adds the index of the highest scoring emotion to target where (0, 1, 2, 3, 4, 5) = (anger, disgust, fear, joy, sadness, surprise)
        score_vector = sentences_test[i][0]
        test_y_targets.append(score_vector.index(sorted(score_vector, reverse = True)[0]))


    #train Gaussian Naive Bayes model
    model = GaussianNB()
    model.fit(train_x_vectors, train_y_targets)

    #model scoring for training set
    train_y_pred = model.predict(train_x_vectors)
    print("Model precision for the training set: " + str(common.get_precision(train_y_pred, train_y_targets)))
    print("Model recall for the training set: " + str(common.get_recall(train_y_pred, train_y_targets)))
    print("Model F-score for the training set: " + str(common.get_f_measure(train_y_pred, train_y_targets)))

    #model scoring for test set
    test_y_pred = model.predict(test_x_vectors)
    print("Model precision for the test set: " + str(common.get_precision(test_y_pred, test_y_targets)))
    print("Model recall for the test set: " + str(common.get_recall(test_y_pred, test_y_targets)))
    print("Model F-score for the test set: " + str(common.get_f_measure(test_y_pred, test_y_targets)))

    return (model, lexicon_global)