import os
from sklearn.naive_bayes import GaussianNB
from library.common import common

def get_local_vocabulary(dataset):
    vocabulary = []
    for group in dataset:
        for sentence_id in dataset[group]:
            bow = dataset[group][sentence_id][0]
            for word in bow:
                vocabulary.append(word)

    return set(vocabulary)

def get_maximizing_index(l):
    max_value = 0
    max_index = 0
    for index in range(len(l)):
        if l[index] > max_value:
            max_value = l[index]
            max_index = index

    return max_index

def get_ordered_vectors(dataset, lexicon, model_type):
    vectors = {}
    for group in dataset:
        x_vectors, y_targets = [], []
        for sentence_id in dataset[group]:
            bow = dataset[group][sentence_id][0]
            score = dataset[group][sentence_id][1]
            x_vector = common.bow_to_vector(bow, lexicon)

            if model_type == 'six_emotion':
                y_target = get_maximizing_index(score)
            if model_type == 'two_emotion':
                y_target = score

            x_vectors.append(x_vector)
            y_targets.append(y_target)

        vectors[group+'_x_vectors'] = x_vectors
        vectors[group+'_y_targets'] = y_targets

    return vectors

def build_model_data(select_datasets, model_type, datasets, lexicon):
    vector_datasets = {}
    for dataset in select_datasets:
        vector_datasets[dataset] = get_ordered_vectors(datasets[dataset], lexicon, model_type)

    model_data = {'all_train_x_vectors': [], 'all_train_y_targets': [], 'all_test_x_vectors': [], 'all_test_y_targets': []}
    for dataset in vector_datasets:
        model_data['all_train_x_vectors'].extend(vector_datasets[dataset]['train_x_vectors'])
        model_data['all_train_y_targets'].extend(vector_datasets[dataset]['train_y_targets'])
        model_data['all_test_x_vectors'].extend(vector_datasets[dataset]['test_x_vectors'])
        model_data['all_test_y_targets'].extend(vector_datasets[dataset]['test_y_targets'])

    return model_data

def build_model(model_data):
    #train Gaussian Naive Bayes model
    model = GaussianNB()
    model.fit(model_data['all_train_x_vectors'], model_data['all_train_y_targets'])

    #model scoring for training set
    train_y_pred = model.predict(model_data['all_train_x_vectors'])
    print("Model precision for the training set: " + str(common.get_precision(train_y_pred, model_data['all_train_y_targets'])))
    print("Model recall for the training set: " + str(common.get_recall(train_y_pred, model_data['all_train_y_targets'])))
    print("Model F-score for the training set: " + str(common.get_f_measure(train_y_pred, model_data['all_train_y_targets'])))

    #model scoring for test set
    test_y_pred = model.predict(model_data['all_test_x_vectors'])
    print("Model precision for the test set: " + str(common.get_precision(test_y_pred, model_data['all_test_y_targets'])))
    print("Model recall for the test set: " + str(common.get_recall(test_y_pred, model_data['all_test_y_targets'])))
    print("Model F-score for the test set: " + str(common.get_f_measure(test_y_pred, model_data['all_test_y_targets'])))

    return model

def __main__(MAINDIR, datasets, two_emotion_datasets, six_emotion_datasets):
    os.chdir(MAINDIR + '/data/model_train/sentiment')

    #build the global lexicon
    print("Building global lexicon")
    global_lexicon = {}
    for dataset in datasets:
        local_vocabulary = get_local_vocabulary(datasets[dataset])
        for word in local_vocabulary:
            if word not in global_lexicon.keys():
                global_lexicon[word] = len(global_lexicon)

    print("Done\n")

    models = {}

    #build two emotion model data
    if len(two_emotion_datasets) > 0:
        print("Building two emotion model data")
        two_emotion_model_data = build_model_data(two_emotion_datasets, 'two_emotion', datasets, global_lexicon)
        print("Done\n")
        print("Building two emotion model")
        two_emotion_model = build_model(two_emotion_model_data)
        models['two_emotion'] = two_emotion_model
        print("Done\n")

    #build models
    if len(six_emotion_datasets) > 0:
        print("Building six emotion model data")
        six_emotion_model_data = build_model_data(six_emotion_datasets, 'six_emotion', datasets, global_lexicon)
        print("Done\n")
        print("Building six emotion model")
        six_emotion_model = build_model(six_emotion_model_data)
        models['six_emotion'] = six_emotion_model
        print("Done\n")

    return [models, global_lexicon]