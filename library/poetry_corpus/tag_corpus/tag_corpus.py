import os
import json
from library.common import common

def parse_poetry_corpus(corpus):
    poetry_set = {}
    with open(corpus) as file:
        for line in file:
            obj = json.loads(line)
            if int(obj['gid']) not in poetry_set.keys():
                poetry_set[int(obj['gid'])] = obj['s'].lower()
                print("Processing poem %s" %(obj['gid']))
            else:
                poetry_set[int(obj['gid'])] += " " + obj['s'].lower()

    file.close()

    return poetry_set

def __main__(MAINDIR, models, global_lexicon):
    os.chdir(MAINDIR + '/data/poetry_corpus')

    corpus_name = 'gutenberg-poetry.subsubsample.ndjson'

    poetry_set = parse_poetry_corpus(os.getcwd()+'/'+corpus_name)

    poetry_set_vectors = {}
    for poetry_id in poetry_set:
        sentence = poetry_set[poetry_id]
        bow = common.sentence_to_bow(sentence)
        vector = common.bow_to_vector(bow, global_lexicon)
        poetry_set_vectors[poetry_id] = vector
    
    sorted_poetry_set_ids = sorted(list(poetry_set_vectors.keys()))
    x_vectors = []
    for i in sorted_poetry_set_ids:
        x_vectors.append(poetry_set_vectors[i])

    print("predicting now")
    model_predictions = {}
    for model_name in models:
        model = models[model_name]
        model_predictions[model_name] = model.predict(x_vectors)

    with open(os.getcwd()+'/'+corpus_name+'.tagged', 'w') as file:
        file.write("#tags for corpus: "+corpus_name+"\n#corpus file location: "+os.getcwd()+'/'+corpus_name+"\n#tag formatting; gutenberg_id")
        #six emotion model: emotional_tag\n#emotion representation; (0, 1, 2, 3, 4, 5) = (anger, disgust, fear, joy, sadness, surprise)
        for model_name in model_predictions:
            file.write(" : " + model_name + " predictions")
        file.write('\n')

        for j in range(len(sorted_poetry_set_ids)):
            file.write(str(sorted_poetry_set_ids[j]))
            for model_name in model_predictions:
                y_pred = model_predictions[model_name]
                file.write(" : " + str(y_pred[j]))
            file.write("\n")

    print("Wrote to "+os.getcwd()+'/'+corpus_name+'.tagged')

    file.close()