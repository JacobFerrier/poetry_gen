import os
from library.common import common

def __main__(MAINDIR, models, global_lexicon, haikus):
    # build haiku vectors out of bow models for individual haikus, also add a list of models used to tag haikus for later
    haiku_vectors = []
    for i in range(len(haikus)):
        haiku_vectors.append(common.bow_to_vector(haikus[i]['bow'], global_lexicon))
        haikus[i]['models'] = models
    
    # tag haikus with every model in models
    for model_name in models:
        print("Making predictions for " + str(model_name))
        model = models[model_name]
        predictions = model.predict(haiku_vectors)
        for j in range(len(predictions)):
            haikus[j][model_name+'_prediction'] = predictions[j]

        print('Done\n')

    return haikus