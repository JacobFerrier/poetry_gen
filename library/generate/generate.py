import os, random
from library.model_train.structure import structure

def __main__(MAINDIR, structure_models, model_name, condition, subject, num_haikus):
    # get the subset of structures and haikus based on user input model_name and condition
    structures = structure_models[str(model_name) + '_' + str(condition)]['structures']
    haikus = structure_models[str(model_name) + '_' + str(condition)]['haikus']

    for i in range(num_haikus):
        print("\n###\nHaiku number %i\n###\n" %(i))
        # initiaize join_probability to 1
        joint_probability = 1

        # get the first line structure of haiku by randomly selecting a structure from the structures dictionary
        structure_index = list(structures.keys())[random.randint(0, len(structures)-1)]
        first_structure = structures[structure_index]['structure']
        joint_probability = structures[structure_index]['unigram_probability']

        # get the second line structure of haiku by finding the next most likely structure following the first
        second_out = structure.get_most_likely_following_structure(first_structure, haikus, structures)
        second_structure = second_out[0]
        joint_probability *= second_out[1]

        # get the third line structure of haiku by finding the next most likely structures following the second
        third_out = structure.get_most_likely_following_structure(second_structure, haikus, structures)
        third_structure = third_out[0]
        joint_probability *= third_out[1]

        # print haiku structure and joint probability of the three lines to console
        print('Haiku structure:')
        print(first_structure)
        print(second_structure)
        print(third_structure)
        print('\nThis haiku structure appears with probability %f based on the input data\n' %(joint_probability))
