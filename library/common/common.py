import re
from nltk import word_tokenize

#get_precision for model
def get_precision(y_pred, y_true):
    true_positives, false_positives = 0, 0
    for i in range(len(y_pred)):
        if y_pred[i] == 1 and y_true[i] == 1:
            true_positives += 1
        elif y_pred[i] == 1 and y_true[i] == 0:
            false_positives += 1
            
    return (true_positives/(true_positives+false_positives))
    
#get_recall for model
def get_recall(y_pred, y_true):
    true_positives, false_negatives = 0, 0
    for i in range(len(y_pred)):
        if y_pred[i] == 1 and y_true[i] == 1:
            true_positives += 1
        elif y_pred[i] == 0 and y_true[i] == 1:
            false_negatives += 1
    
    return (true_positives/(true_positives+false_negatives))
    
#get_f_measure for model by calculating the precision (p) and recall (r) of y_pred and y_true
#then multiplying 2, p and r together and dividing the result by p + r
def get_f_measure(y_pred, y_true):
    p = get_precision(y_pred, y_true)
    r = get_recall(y_pred, y_true)
    
    return ((2*p*r)/(p+r))

def get_valid_token(token):
    if token.isalnum():
        return token
    elif token.startswith("-"):
        return token.replace("-", "")
    elif token.startswith("'") and len(token) > 3:
        return token.replace("'", "")
    elif "-" in token:
        m_s = re.match(r'\w+(-\w+)+', token)
        if m_s:
            return m_s.group()
    elif "," in token:
        m_d = re.match(r'\d+,\d+', token)
        if m_d:
            return m_d.group()
    elif token.count("'") == 1 and len(token) > 3:
        return token

def get_word_vector(string, lexicon):
    string_list = string.split()
    word_list = []
    for word in string_list:
        word = word.lower().strip(';,!.?":').strip()
        word_list.append(word)

    word_counts = {}
    for word in word_list:
        if word not in word_counts.keys(): word_counts[word] = 1
        else: word_counts[word] += 1

    string_bow_form = []
    string_vector_form = [0 for x in range(len(lexicon))]
    for word in word_counts:
        if word in lexicon.keys():
            string_bow_form.append((lexicon[word], word_counts[word]))
            string_vector_form[lexicon[word]] = word_counts[word]

    return (string_bow_form, string_vector_form)

def sentence_to_bow(sentence):
    tokenized = word_tokenize(sentence)
    bow = {}
    for token in tokenized:
        word = get_valid_token(token)
        if word:
            if word in bow.keys():
                bow[word] += 1
            else:
                bow[word] = 1

    return bow

def bow_to_vector(bow, lexicon):
    vector = [0 for x in lexicon]
    for word in bow:
        if word in lexicon.keys():
            word_id = lexicon[word]
            vector[word_id] = 1

    return vector