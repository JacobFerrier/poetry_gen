import os, re, nltk
from library.common import common

def get_sentences_semeval_2007(corpus):
    sentences = {}
    with open(corpus) as file:
        for line in file:
            m_id = re.match(r'<instance id="(\d+)">', line)
            if m_id:
                sentence_id = int(m_id.groups()[0])
                sentence = line.split('>')[1].split('<')[0].strip().lower()
                sentences[sentence_id] = common.sentence_to_bow(sentence)

    return sentences

def get_scores_semeval_2007(corpus):
    sentence_scores = {}
    with open(corpus) as file:
        for line in file:
            seq = line.strip().split()
            score_vector = [ int(x) for x in seq[1:]]
            sentence_scores[int(seq[0])] = score_vector

    return sentence_scores

def get_dataset_semeval_2007(corpus_type):
    dataset = {}
    sentences = get_sentences_semeval_2007(os.getcwd()+'/semeval_2007/AffectiveText.'+corpus_type+'/affectivetext_'+corpus_type+'.xml')
    scores = get_scores_semeval_2007(os.getcwd()+'/semeval_2007/AffectiveText.'+corpus_type+'/affectivetext_'+corpus_type+'.emotions.gold')

    if len(sentences) != len(scores):
        exit("Length of sentences not equal to length of scores")

    for id in sentences.keys():
        dataset[id] = [sentences[id], scores[id]]

    return dataset

def get_dataset_aclimdb(corpus_type):
    dataset = {}
    for e in ['pos', 'neg']:
        for filename in os.listdir(os.getcwd()+'/aclimdb/'+corpus_type+'/'+e+'/'):
            s = filename.split('_')
            sentence_id = s[0]
            sentence_score = int(s[1].strip('.txt'))
            with open(os.getcwd()+'/aclimdb/'+corpus_type+'/'+e+'/'+filename) as file:
                try:
                    sentence = ""
                    for line in file:
                        sentence += line.strip().lower()
                except UnicodeDecodeError:
                    sentence = None
                    pass

            if sentence:
                sentence = sentence.strip()
                sentence = sentence.replace('<br />', ' ')
                dataset[e+'_'+sentence_id] = [common.sentence_to_bow(sentence), sentence_score]


    return dataset

def __main__(MAINDIR):
    os.chdir(MAINDIR + '/data/model_train/sentiment')

    datasets = {}

    #semeval_2007 sentence and score parsing - anger/disgust/fear/joy/sadness/surprise [0-100]
    print("Parsing semeval_2007")
    train_semeval_2007 = get_dataset_semeval_2007('trial')
    test_semeval_2007 = get_dataset_semeval_2007('test')
    datasets['semeval_2007'] = {'train': train_semeval_2007, 'test': test_semeval_2007}
    print("Done\n")

    #aclimdb sentence and score parsing - positive/negative [0-10]
    print("Parsing aclimdb")
    #train_aclimdb = get_dataset_aclimdb('train')
    #test_aclimdb = get_dataset_aclimdb('test')
    #datasets['aclimdb'] = {'train': train_aclimdb, 'test': test_aclimdb}
    print("Done\n")

    common.sentence_to_bow("this test sentence is a test.")

    return datasets