import os
from library.common import common
from nltk import word_tokenize, pos_tag

def parse_herval(corpus):
    with open(corpus) as file:
        haikus = {}
        haikus[0] = {'sentence_structure': [], 'bow': {}, 'pos_structure': []}
        for line in file:
            line = line.strip()
            if line == '':
                if len(haikus[len(haikus)-1]['sentence_structure']) == 0:
                    del haikus[len(haikus)-1]
                else:
                    full_sentence = ''
                    for sentence in haikus[len(haikus)-1]['sentence_structure']:
                        full_sentence += sentence + " "
                        haikus[len(haikus)-1]['pos_structure'].append([x[1] for x in pos_tag(word_tokenize(sentence))])
                    
                    haikus[len(haikus)-1]['bow'] = common.sentence_to_bow(full_sentence.strip())

                haikus[len(haikus)] = {'sentence_structure': [], 'bow': {}, 'pos_structure': []}
            else:
                haikus[len(haikus)-1]['sentence_structure'].append(line)

    if len(haikus[len(haikus)-1]['sentence_structure']) == 0:
        del haikus[len(haikus)-1]

    return haikus
            
def __main__(MAINDIR):
    os.chdir(MAINDIR + '/data/haiku_corpus')

    print("Parsing herval corpus")
    haikus = parse_herval('herval_corpus.txt')
    
    # filter out non three line/sentence haikus
    filtered_haikus = {}
    for haiku in haikus:
        if len(haikus[haiku]['sentence_structure']) == 3:
            filtered_haikus[len(filtered_haikus)] = haikus[haiku]
    print("Done\n")

    return filtered_haikus