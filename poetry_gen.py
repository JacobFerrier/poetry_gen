import os

MAINDIR = os.getcwd()

try:
    print('\n-----Parsing datasets for sentiment models-----\n')
    from library.parse.sentiment import parse
    datasets = parse.__main__(MAINDIR)
    print('-Done-')
except Exception as e:
    print('Exception found during parsing sentiment model datasets: ' + str(e))
    exit()

try:
    print('\n-----Training sentiment models-----\n')
    from library.model_train.sentiment import sentiment
    #two_emotion_datasets = ['aclimdb'] #the pos/neg dataset is not currently implemented
    two_emotion_datasets = []
    six_emotion_datasets = ['semeval_2007']
    out = sentiment.__main__(MAINDIR, datasets, two_emotion_datasets, six_emotion_datasets)
    models = out[0]
    global_lexicon = out[1]
    print('-Done-')
except Exception as e:
    print('Exception found during sentiment model training: ' + str(e))
    exit()

try:
    print('\n-----Building model condition table by manually defined model lookup conditions-----\n')
    model_condition_table = {}
    for model_name in models:
        if model_name == 'two_emotion':
            model_condition_table[model_name] = [0,1]
        elif model_name == 'six_emotion':
            model_condition_table[model_name] = [0,1,2,3,4,5]
    print('-Done-')
except Exception as e:
    print('Exception found during model condition table building: ' + str(e))

try:
    print('\n-----Parsing datasets for haikus-----\n')
    from library.parse.haiku import parse
    haikus = parse.__main__(MAINDIR)
    print('-Done-')
except Exception as e:
    print('Exception found during parsing haiku datasets: ' + str(e))
    exit()

try:
    print('\n-----Tagging haiku corpus-----\n')
    from library.haiku_corpus.tag_corpus import tag_corpus
    haikus_tagged = tag_corpus.__main__(MAINDIR, models, global_lexicon, haikus)
    print('-Done-')
except Exception as e:
    print('Exception found during tagging of haiku corpus: ' + str(e))

try:
    print('\n-----Training structure models for tagged haiku corpus-----\n')
    from library.model_train.structure import structure
    structure_models = structure.__main__(MAINDIR, haikus_tagged, model_condition_table, 0.015)
    print('-Done-')
except Exception as e:
    print('Exception found during structure model training: ' + str(e))

try:
    print('\n-----Generating haiku based on user input-----\n')
    from library.generate import generate
    print('The following model(s) are implemented with the following condition(s):')
    for model_name in models:
        print(model_name)
        if model_name == 'two_emotion':
            print('0 -> negative\n1 -> positive')
        elif model_name == 'six_emotion':
            print('0 -> anger\n1 -> disgust\n2 -> fear\n3 -> joy\n4 -> sadness\n5 -> surprise\n')

    user_continue = 'y'
    while user_continue == 'y':
        user_continue, user_model_name, user_condition, user_subject, user_number = '', '', -1, '', -0.5

        # get the model to target from user input
        while user_model_name not in model_condition_table:
            user_model_name = input('Please type the model you want to generate from (string): ')

        # get the condition of the model to target from user input
        while user_condition not in model_condition_table[user_model_name]:
            user_condition = int(input('Please type the condition you want to target (integer): '))

        # get the subject of the haiku from user input - unimplemented
        # user_subject = input('Please type the target subject of your haiku (string): ')

        # get the number of haikus to generate from user input
        while user_number < 1 and not isinstance(user_number, int):
            user_number = int(input('Please type the number of haikus to generate with the previously provided parameters (integer): '))
        
        # generate the haiku(s) from user input
        generate.__main__(MAINDIR, structure_models, user_model_name, user_condition, user_subject, user_number)

        # determine if the loop should continue based on user input
        while user_continue not in ['y', 'n']:
            user_continue = input('Please type if you want to continue or not (y/n): ')

    print('\n-Done-')
except Exception as e:
    print('Exception found during generating haiku based on user input: ' + str(e))


# all of the code below is not required in the current implementation
#try:
#    print('\n-----Tagging poetry corpus-----\n')
#    from library.poetry_corpus.tag_corpus import tag_corpus
#    #tag_corpus.__main__(MAINDIR, models, global_lexicon)
#    print('-Done-')
#except Exception as e:
#    print('Exception found during tagging of poetry corpus: ' + str(e))
#    exit()

print('\nPipeline complete\n')